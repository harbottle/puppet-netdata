# Swap file
Service {
  require => Class['swap::config']
}
class { 'swap::config': }

# Firewall
Firewall {
  before  => Class['my_fw::post'],
  require => Class['my_fw::pre'],
}
class { ['my_fw::pre', 'my_fw::post']: }
class { 'firewall': }
firewall { '100 allow netdata TCP 19999':
    dport  => [19999],
    proto  => tcp,
    action => accept,
}

# netdata
if ($::osfamily == 'RedHat') {
  $yum = true
  $group = 'netdata'
}
else {
  $yum = false
  $group = 'root'
}
class { 'netdata':
  manage_yumrepo => $yum,
  main_cfg       => {
    'global' => {
      'run as user' => 'netdata',
      'history'     => 3600,
      'bind to'     => '*'
    },
    'web' => {
      'web files owner' => 'root',
      'web files group' => 'root'
    }
  }
}
