class { 'netdata':
  manage_yumrepo              => true,
  yumrepo                     => {
    'local-netdata-repo'      => {
      baseurl  => 'https://yum.local/local-netdata-repo',
      descr    => 'Local netdata repo',
      enabled  => true,
      gpgcheck => false
    }
  },

  manage_package              => true,
  package_name                => 'my_netdata',
  package_ensure              => '1.10.0',

  manage_cfg_dir              => true,
  cfg_dir                     => '/opt/netdata/etc',

  manage_main_cfg             => true,
  main_cfg_file               => '/opt/netdata/etc/netdata.conf',
  main_cfg                    => {
    'global' => {
      'run as user' => 'my_netdata',
      'history'     => 3600,
      'bind to'     => '*'
    },
    'web'    => {
      'web files owner' => 'root',
      'web files group' => 'my_netdata'
    }
  },

  manage_apps_groups_cfg      => true,
  apps_groups_cfg_file        => '/opt/netdata/etc/apps_groups.conf',
  apps_groups_cfg             => [
    'netdata: netdata',
    'apps.plugin: apps.plugin',
    'freeipmi.plugin: freeipmi.plugin',
    'charts.d.plugin: *charts.d.plugin*',
    'node.d.plugin: *node.d.plugin*',
    'python.d.plugin: *python.d.plugin*',
    'tc-qos-helper: *tc-qos-helper.sh*',
    'fping: fping',
    'httpd: apache* httpd nginx* lighttpd',
    'corporate: foo* bar* *bat*'
  ],

  manage_charts_d_cfg         => true,
  charts_d_cfg_file           => '/opt/netdata/etc/charts.d.conf',
  charts_d_cfg                => {
    'time_divisor' => '100',
    'opensips'     => 'no'
  },

  manage_fping_cfg            => true,
  fping_cfg_file              => '/opt/netdata/etc/fping.conf',
  fping_bin                   => '/usr/local/bin/fping',
  fping_hosts                 => [
    'web.local',
    '192.168.2.3'
  ],
  fping_update_every          => '4',
  fping_ping_every            => '400',
  fping_opts                  => '-R -b 56 -i 1 -r 0 -t 5000',

  manage_node_d_cfg           => true,
  node_d_cfg_file             => '/opt/netdata/etc/node.d.conf',
  node_d_cfg                  => {
    'update_every' => 5,
    'modules'      => {
      'named'      => {
        'enabled' => true
      },
      'sma_webbox' => {
        'enabled' => true
      },
      'snmp'       => {
        'enabled' => false
      }
    }
  },

  manage_python_d_cfg         => true,
  python_d_cfg_file           => '/opt/netdata/etc/python.d.conf',
  python_d_cfg                => {
    'enabled' => 'no'
  },

  manage_stream_cfg           => true,
  stream_cfg_file             => '/opt/netdata/etc/stream.conf',
  stream_enabled              => true,
  stream_destination          => 'updstream.local',
  stream_api_key              => 'my_sending_api_key',
  stream_timeout              => 120,
  stream_default_port         => 19998,
  stream_buffer_size_bytes    => 1048576,
  stream_reconnect_delay      => 10,
  stream_initial_clock_resync => 30,
  stream_options_per_id       => {
    'api_key_1' => {
      'enabled' => 'no'
    }
  },

  manage_service              => true,
  service_name                => 'my_netdata',
  ensure_service              => 'running',
  enable_service              => false,
}