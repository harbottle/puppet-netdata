# netdata

[![Puppet Forge - downloads](https://img.shields.io/puppetforge/dt/liger1978/netdata.svg)](https://forge.puppetlabs.com/liger1978/netdata)
[![Puppet Forge - scores](https://img.shields.io/puppetforge/f/liger1978/netdata.svg)](https://forge.puppetlabs.com/liger1978/netdata)
[![GitLab - build status](https://gitlab.com/harbottle/puppet-netdata/badges/master/build.svg)](https://gitlab.com/harbottle/puppet-netdata/builds)

#### Table of Contents

1. [Description](#description)
2. [Beginning with netdata](#beginning-with-netdata)
3. [Usage - Configuration options and additional functionality](#usage)
4. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)
6. [Development - Guide for contributing to the module](#development)

## Description

This module installs and configures [netdata](https://my-netdata.io/). It is currently designed to work only with RHEL/CentOS 7 and Ubuntu 18.04. For Red Hat systems, it uses the RPM package available in the [harbottle-main repository](https://harbottle.gitlab.io/harbottle-main/7/x86_64/).

The netdata module can do the following:
* Install a yum repo for netdata.
* Install the netdata package.
* Comprehensively manage the main netdata configuration files.
* Manage the netdata service.

## Beginning with netdata

For an internet-connected server, this is the minimum required to install, configure and run netdata using the netdata package from the [harbottle-main repository](https://harbottle.gitlab.io/harbottle-main/7/x86_64/).

Using Puppet only:

```puppet
class { 'netdata':
  manage_yumrepo => true,
}
```

Using Puppet and Hiera:

```puppet
include netdata
```

```yaml
---
netdata::manage_yumrepo: true
```

## Usage

Here is a comprehensive example using many of the available parameters to extensively modify module behaviour.

Using Puppet only:

```puppet
class { 'netdata':
  manage_yumrepo              => true,
  yumrepo                     => {
    'local-netdata-repo'      => {
      baseurl  => 'https://yum.local/local-netdata-repo',
      descr    => 'Local netdata repo',
      enabled  => true,
      gpgcheck => false
    }
  },

  manage_package              => true,
  package_name                => 'my_netdata',
  package_ensure              => '1.10.0',

  manage_cfg_dir              => true,
  cfg_dir                     => '/opt/netdata/etc',

  manage_main_cfg             => true,
  main_cfg_file               => '/opt/netdata/etc/netdata.conf',
  main_cfg                    => {
    'global' => {
      'run as user' => 'my_netdata',
      'history'     => 3600,
      'bind to'     => '*'
    },
    'web'    => {
      'web files owner' => 'root',
      'web files group' => 'my_netdata'
    }
  },

  manage_apps_groups_cfg      => true,
  apps_groups_cfg_file        => '/opt/netdata/etc/apps_groups.conf',
  apps_groups_cfg             => [
    'netdata: netdata',
    'apps.plugin: apps.plugin',
    'freeipmi.plugin: freeipmi.plugin',
    'charts.d.plugin: *charts.d.plugin*',
    'node.d.plugin: *node.d.plugin*',
    'python.d.plugin: *python.d.plugin*',
    'tc-qos-helper: *tc-qos-helper.sh*',
    'fping: fping',
    'httpd: apache* httpd nginx* lighttpd',
    'corporate: foo* bar* *bat*'
  ],

  manage_charts_d_cfg         => true,
  charts_d_cfg_file           => '/opt/netdata/etc/charts.d.conf',
  charts_d_cfg                => {
    'time_divisor' => '100',
    'opensips'     => 'no'
  },

  manage_fping_cfg            => true,
  fping_cfg_file              => '/opt/netdata/etc/fping.conf',
  fping_bin                   => '/usr/local/bin/fping',
  fping_hosts                 => [
    'web.local',
    '192.168.2.3'
  ],
  fping_update_every          => '4',
  fping_ping_every            => '400',
  fping_opts                  => '-R -b 56 -i 1 -r 0 -t 5000',

  manage_node_d_cfg           => true,
  node_d_cfg_file             => '/opt/netdata/etc/node.d.conf',
  node_d_cfg                  => {
    'update_every' => 5,
    'modules'      => {
      'named'      => {
        'enabled' => true
      },
      'sma_webbox' => {
        'enabled' => true
      },
      'snmp'       => {
        'enabled' => false
      }
    }
  },

  manage_python_d_cfg         => true,
  python_d_cfg_file           => '/opt/netdata/etc/python.d.conf',
  python_d_cfg                => {
    'enabled' => 'no'
  },

  manage_stream_cfg           => true,
  stream_cfg_file             => '/opt/netdata/etc/stream.conf',
  stream_enabled              => true,
  stream_destination          => 'upstream.local',
  stream_api_key              => 'my_sending_api_key',
  stream_timeout              => 120,
  stream_default_port         => 19998,
  stream_buffer_size_bytes    => 1048576,
  stream_reconnect_delay      => 10,
  stream_initial_clock_resync => 30,
  stream_options_per_id       => {
    'api_key_1' => {
      'enabled' => 'no'
    }
  },

  manage_service              => true,
  service_name                => 'my_netdata',
  ensure_service              => 'running',
  enable_service              => false,
}
```

Using Puppet and Hiera:

```puppet
include netdata
```

```yaml
---
netdata::manage_yumrepo: true
netdata::yumrepo:
  local-netdata-repo:
    baseurl: https://yum.local/local-netdata-repo
    descr: Local netdata repo
    enabled: true
    gpgcheck: false

netdata::manage_package: true
netdata::package_name: my_netdata
netdata::package_ensure: '1.10.0'

netdata::manage_cfg_dir: true
netdata::cfg_dir: /opt/netdata/etc

netdata::manage_main_cfg: true
netdata::main_cfg_file: /opt/netdata/etc/netdata.conf
netdata::main_cfg:
  global:
    run as user: my_netdata
    history: 3600
    bind to: '*'
  web:
    web files owner: root
    web files group: my_netdata

netdata::manage_apps_groups_cfg: true
netdata::apps_groups_cfg_file: /opt/netdata/etc/apps_groups.conf
netdata::apps_groups_cfg:
 - 'netdata: netdata'
 - 'apps.plugin: apps.plugin'
 - 'freeipmi.plugin: freeipmi.plugin'
 - 'charts.d.plugin: *charts.d.plugin*'
 - 'node.d.plugin: *node.d.plugin*'
 - 'python.d.plugin: *python.d.plugin*'
 - 'tc-qos-helper: *tc-qos-helper.sh*'
 - 'fping: fping'
 - 'httpd: apache* httpd nginx* lighttpd'
 - 'corporate: foo* bar* *bat*'

netdata::manage_charts_d_cfg: true
netdata::charts_d_cfg_file: /opt/netdata/etc/charts.d.conf
netdata::charts_d_cfg:
  time_divisor: '100'
  opensips: 'no'

netdata::manage_fping_cfg: true
netdata::fping_cfg_file: /opt/netdata/etc/fping.conf
netdata::fping_bin: /usr/local/bin/fping
netdata::fping_hosts: 
 - web.local
 - 192.168.2.3
netdata::fping_update_every: '4'
netdata::fping_ping_every: '400'
netdata::fping_opts: -R -b 56 -i 1 -r 0 -t 5000

netdata::manage_node_d_cfg: true
netdata::node_d_cfg_file: /opt/netdata/etc/node.d.conf
netdata::node_d_cfg:
  update_every: 5
  modules:
    named:
      enabled: true
    sma_webbox:
      enabled: true
    snmp:
      enabled: false

netdata::manage_python_d_cfg: true,
netdata::python_d_cfg_file: /opt/netdata/etc/python.d.conf
netdata::python_d_cfg: 
  enabled: 'no'

netdata::manage_stream_cfg: true
netdata::stream_cfg_file: /opt/netdata/etc/stream.conf
netdata::stream_enabled: true
netdata::stream_destination: updstream.local
netdata::stream_api_key: my_sending_api_key
netdata::stream_timeout: 120
netdata::stream_default_port: 19998
netdata::stream_buffer_size_bytes: 1048576
netdata::stream_reconnect_delay: 10
netdata::stream_initial_clock_resync: 30
netdata::stream_options_per_id: 
  api_key_1:
    enabled: 'no'

netdata::manage_service: true
netdata::service_name: my_netdata
netdata::ensure_service: running
netdata::enable_service: false
```

## Reference

Reference documentation is generated using [Puppet Strings](https://github.com/puppetlabs/puppet-strings).

If you are reading this at the Puppet Forge, you should find what you are looking for under the `Reference` tab for this module, otherwise look for `REFERENCE.md` in the same directory as this file.

## Limitations

This module has been tested with Puppet 3, 4 and 5.

This module has been tested on:

* Red Hat Enterprise Linux 7
* CentOS 7
* Oracle Linux 7
* Scientific Linux 7
* Ubuntu 18.04

## Development

Please open an issue, indicate you intend to work on it and create a GitLab merge request.