# Private class for managing the netdata package repository.
#
# @summary Private class for managing the netdata package repository.
#
# @api private
#
# @example
#   include netdata::repo
class netdata::repo {
  if ($netdata::manage_yumrepo) and ($::osfamily == 'RedHat') and (!empty($netdata::yumrepo)) {
    ensure_resources('yumrepo', $netdata::yumrepo)
  }
}
