# Private class for managing netdata configuration.
#
# @summary Private class for managing netdata configuration.
#
# @api private
#
# @example
#   include netdata::config
class netdata::config {
  $cfg_dir = $netdata::cfg_dir
  $main_cfg_file = regsubst($netdata::main_cfg_file,'/__DIR__',$cfg_dir)
  $apps_groups_cfg_file = regsubst($netdata::apps_groups_cfg_file,'/__DIR__',$cfg_dir)
  $charts_d_cfg_file = regsubst($netdata::charts_d_cfg_file,'/__DIR__',$cfg_dir)
  $fping_cfg_file = regsubst($netdata::fping_cfg_file,'/__DIR__',$cfg_dir)
  $node_d_cfg_file = regsubst($netdata::node_d_cfg_file,'/__DIR__',$cfg_dir)
  $python_d_cfg_file = regsubst($netdata::python_d_cfg_file,'/__DIR__',$cfg_dir)
  $stream_cfg_file = regsubst($netdata::stream_cfg_file,'/__DIR__',$cfg_dir)

  if $netdata::manage_cfg_dir {
    file { $cfg_dir:
      ensure => 'directory',
    }
  }
  if $netdata::manage_main_cfg {
    file { $main_cfg_file:
      content => template('netdata/netdata.conf.erb'),
    }
  }
  if $netdata::manage_apps_groups_cfg {
    file { $apps_groups_cfg_file:
      content => template('netdata/apps_groups.conf.erb'),
    }
  }
  if $netdata::manage_charts_d_cfg {
    file { $charts_d_cfg_file:
      content => template('netdata/charts.d.conf.erb'),
    }
  }
  if $netdata::manage_fping_cfg {
    file { $fping_cfg_file:
      content => template('netdata/fping.conf.erb'),
    }
  }
  if $netdata::manage_node_d_cfg {
    file { $node_d_cfg_file:
      content => to_json_pretty($netdata::node_d_cfg),
    }
  }
  if $netdata::manage_python_d_cfg {
    file { $python_d_cfg_file:
      content => to_yaml($netdata::python_d_cfg),
    }
  }
  if $netdata::manage_stream_cfg {
    case $netdata::stream_enabled {
      true:    { $stream_enabled = 'yes' }
      default: { $stream_enabled = 'no' }
    }
    file { $stream_cfg_file:
      content => template('netdata/stream.conf.erb'),
    }
  }
}
