# Private class providing default values for netdata parameters.
#
# @summary Private class providing default values for netdata parameters.
#
# @api private
#
# @example
#   include netdata::params
class netdata::params {
  $fail_msg = "OS ${::operatingsystem} ${::operatingsystemrelease} is not supported"

  $manage_yumrepo = false
  case $::osfamily {
    'RedHat': {
      case $::operatingsystemmajrelease {
        '7': {
          $yumrepo = {
            'harbottle-main' => {
              baseurl       => 'https://copr-be.cloud.fedoraproject.org/results/harbottle/main/epel-7-$basearch/',
              descr         => 'Copr repo for main owned by harbottle',
              enabled       => true,
              gpgcheck      => true,
              gpgkey        => 'https://copr-be.cloud.fedoraproject.org/results/harbottle/main/pubkey.gpg',
              repo_gpgcheck => false
            }
          }
        }
        default: { fail($fail_msg) }
      }
    }
    'Debian': {
      case $::operatingsystem {
        'Ubuntu': {
          case $::operatingsystemmajrelease {
            '18.04': { $yumrepo = {} }
            default: { fail($fail_msg) }
          }
        }
        default: { fail($fail_msg)}
      }
    }
    default: { fail($fail_msg) }
  }

  $manage_package = true
  $package_name = 'netdata'
  $package_ensure = 'latest'

  $manage_cfg_dir = true
  $cfg_dir = '/etc/netdata'

  $manage_main_cfg = true
  $main_cfg_file = '/__DIR__/netdata.conf'
  $main_cfg = {
    'global' => {
      'run as user' => 'netdata',
      'history'     => 3600,
      'bind to'     => 'localhost'
    },
    'web' => {
      'web files owner' => 'root',
      'web files group' => 'netdata'
    }
  }

  $manage_apps_groups_cfg = false
  $apps_groups_cfg_file = '/__DIR__/apps_groups.conf'
  $apps_groups_cfg = []

  $manage_charts_d_cfg = false
  $charts_d_cfg_file = '/__DIR__/charts.d.conf'
  $charts_d_cfg = {}

  $manage_fping_cfg = false
  $fping_cfg_file = '/__DIR__/fping.conf'
  $fping_bin = 'absent'
  $fping_hosts = []
  $fping_update_every = 'absent'
  $fping_ping_every = 'absent'
  $fping_opts = 'absent'

  $manage_node_d_cfg = false
  $node_d_cfg_file = '/__DIR__/node.d.conf'
  $node_d_cfg = {}

  $manage_python_d_cfg = false
  $python_d_cfg_file = '/__DIR__/python.d.conf'
  $python_d_cfg = {}

  $manage_stream_cfg = true
  $stream_cfg_file = '/__DIR__/stream.conf'
  $stream_enabled = false
  $stream_destination = ''
  $stream_api_key = ''
  $stream_timeout = 60
  $stream_default_port = 19999
  $stream_buffer_size_bytes = 1048576
  $stream_reconnect_delay = 5
  $stream_initial_clock_resync = 60
  $stream_options_per_id = {}

  $manage_service = true
  $service_name = 'netdata'
  $ensure_service = 'running'
  $enable_service = true
}
